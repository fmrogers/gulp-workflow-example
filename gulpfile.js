/**
 * @package Gulp Intro
 * @description A simple introdution demo for a Gulp workflow.
 */

/**
 * = Gulp specific dependencies = */
const gulp          = require('gulp');
const rename        = require('gulp-rename');
const autoprefixer  = require('gulp-autoprefixer');
const sourcemaps    = require('gulp-sourcemaps');


/** = CSS STYLE TASK - w/specific dependencies
/** *******************************************/
const sass          = require('gulp-sass');
/** *******************************************/

const styleSrc      = 'src/scss/style.scss';    // Omit the './' to allow gulp watch to register new files
const styleDist     = './assets/css/';

gulp.task('style', () => {
    gulp.src(styleSrc)
        .pipe(sourcemaps.init())
        .pipe(sass({
            errorLogToConsole: true,
            outputStyle: 'compressed'
        }))
        .on('error', console.error.bind(console))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(styleDist));
});


/** = JAVASCRIPT TASK - w/specific dependencies
/** ********************************************/
const browserify    = require('browserify');
const babelify      = require('babelify');
const source        = require('vinyl-source-stream');
const buffer        = require('vinyl-buffer');
const uglify        = require('gulp-uglify');
/** ********************************************/

const jsSrc     = 'script.js';                  // 'jsSrc' unlike sass should only point to the entry file
const jsFolder  = './src/js/';                  // 'jsFolder' points to the location of entry file(s)
const jsFiles   = [jsSrc];                      // Array which can include additional source files.
const jsDist    = './assets/js/';

gulp.task('js', () => {
    jsFiles.map((jsFile) => {                   // The 'jsFile' param represent the current element being processed in the array 'jsFiles'
        return browserify({
            entries: [jsFolder + jsFile]        // Concatenating 'jsFolder' and 'jsFile' prevents task from recreating entire folder structure eg. './src/js/'
        })
        .transform(babelify, {
            presets: ['env']
        })
        .bundle()
        .pipe(source(jsFile))
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(buffer())
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(jsDist))
    });
});


/** = DEFAULT TASK - w/specific dependencies
/** *****************************************/
gulp.task('default', ['style', 'js']);

/** = WATCH TASK - w/specific dependencies
/** *****************************************/
/** *****************************************/

const styleWatch    = 'src/scss/**/*';        // Omit the './' to allow gulp watch to register new files
const jsWatch       = './src/js/**/*';

gulp.task('watch', ['default'], () => {
    gulp.watch(styleWatch, ['style']);
    gulp.watch(jsWatch, ['js']);
});
